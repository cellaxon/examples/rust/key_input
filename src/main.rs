use std::io;

fn main()
{
    let mut user_input = String::new();
    
    println!("What is your name?");

    let _result = io::stdin().read_line(&mut user_input);

    let input = user_input.replace("\r", "").replace("\n", "");
    println!("Hello {}!!",  input);
}

